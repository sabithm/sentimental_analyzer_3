#! /usr/bin/env python

import rospy

import actionlib

import sentimental_analyzer_3.msg



class SpeechAction(object):
    # create messages that are used to publish feedback/result
    _feedback = sentimental_analyzer_3.msg.SpeechFeedback()
    _result =sentimental_analyzer_3.msg.SpeechResult()

    def __init__(self, name):
        self._action_name = name
        self._as = actionlib.SimpleActionServer(self._action_name, sentimental_analyzer_3.msg.SpeechAction, execute_cb=self.execute_cb, auto_start = False)
        self._as.start()
      
    def execute_cb(self, goal):
        # helper variables
        r = rospy.Rate(1)
        success = True
        
        
        # publish info to the console for the user
        rospy.loginfo('performing sentimental analysis of text %s',goal.text)
        x=goal.text
       
        # check that preempt has not been requested by the client
        if self._as.is_preempt_requested():
                rospy.loginfo('%s: Preempted' % self._action_name)
                self._as.set_preempted()
                success = False
           
           #Three different lists that contain the possible words
	positive_words=[
            	 "good morning",
                 "fantastic",
       	         "perfect"
             	 ]
	negative_words=[
             	"kill",
             	"attack",
             	"hell"
              	]

	neutral_words=[
            	"hello",
            	"show",
            	"she"
             	]

        
        for i in range(0,3):             
            if x==positive_words[i]:
                self._feedback.sequence='It is a positive word'
                self._as.publish_feedback(self._feedback)
            # this step is not necessary, the sequence is computed at 1 Hz for demonstration purposes 
            if x==negative_words[i]:
                self._feedback.sequence='It is a negative word'
                self._as.publish_feedback(self._feedback) 
            if x==neutral_words[i]:
                self._feedback.sequence='It is a neutral word'
                self._as.publish_feedback(self._feedback)     
        r.sleep()
          
        if success:
            self._result.sequence = self._feedback.sequence
            rospy.loginfo('%s: Succeeded' % self._action_name)
            self._as.set_succeeded(self._result)
        
if __name__ == '__main__':
    rospy.init_node('speech')
    server = SpeechAction(rospy.get_name())
    rospy.spin()
